<#
.NAME
    ssh-copy-id.ps1
.SYNOPSIS
    ssh-copy-id [-i [identity_file]] [user@]machine
.DESCRIPTION
    Trying to copy the function of the Unix script with the same name. ssh-copy-id is a script that
    uses OpenSSH to log into a remote machine. It also changes the permissions of the remote user's
    home, ~/.ssh, and ~/.ssh/authorized_keys to remove group writabitility. If the -i option is
    given then the identity file is used, regardless of whether there are any keys in your
    ssh-agent. Otherwise, if this: ssh-add -L provides any output, it uses that in preference to
    the identity file. If the -i option is used, or the ssh-add produced no output, then it uses the
    contents fo the identity file. Once it has one or more fingerprints, it uses ssh to append them
    to ~/.ssh/authorized_keys on the remote machine (creating the file, and directory, f necessary)
.NOTES
    AUTHOR:
    DATE: 11-22-2021
    DEPENDENCIES:
        ssh.exe
#>


param(
        [Parameter(HelpMessage="Public key file")] [ValidateScript({Test-path $p})] [Alias("i")]
        [String]$pub,
        [Parameter(HelpMessage="Port number")] [Alias("p")] [String]$port,
        [Parameter(HelpMessage="User and Server as: user@server",Position=0,Mandatory=$true)] [String]$server
    )


    Function Get-OpenSSHOption{
        $bash = "umask 077; test -d .ssh || mkdir .ssh ; cat >> .ssh/authorized_keys"
        if ($null -eq $port){$port = '22'}
        $pub | ssh.exe -p $port $server $bash
    }


try{
    If ((Test-Path $env:USERPROFILE\.ssh\*.pub) -and ($null -eq $pub)){
        $pub = Get-Content $env:USERPROFILE\.ssh\*.pub
        Get-OpenSSHOption
    } 
    elseif (Test-Path $pub){
        $pub = Get-Content $pub
        Get-OpenSSHOPtion
    } else {
        Write-Host "CANNOT FIND PUBLIC KEY: genterate the key with ssh-keygen.exe and place in .ssh/ or use -i"
    }

} catch{
    Write-Error "$($p.Exception.Message)"
}